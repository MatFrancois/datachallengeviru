# Markdoun lasso ETIENNE HAMARD

Etant donnée que nous sommes dans un contexte de biostatistique et que nous avons un nombre de variables très supérieur au nombre d'individus. Nous avons décidé d'utiliser la méthode logistique par pénalisation Lasso (recommandé dans le cadre de la biostatistique car contrairement au ridge, il ne considère pas que toutes les variables ont un effet sur la variable à prédire). 

Nous décidons d'entrainer notre modèle uniquement sur les individus qui n'ont pas de NA dans la variable **mort à 24 mois**, soit 343 individus.


Ayant rencontré un problème lors de la prédiction suivant cette régression, nous nous sommes adaptés à celui ci. La régression a été testé pour plusieurs lambda, mais à chaque fois le lambda était optimal lorsque tous les coéficients étaient égals à 0. Cela voudrait dire qu'aucune définition de gêne ne permet de prédire la mort à 24 mois d'un patient. Mais nous avons remarqué que ces résultats étaient très aléatoire, en effet en répétant 100 fois ce modèle nous obtenons certaines variables qui apparaissent dans plus de 10% des cas. 

En gardant uniquement ces variables (environ 20), nous leur appliquons une régression logistique classique, suivit d'une optimisation pas à pas par méthode backward. Nous avons donc le modèle suivant : LVRN + TRPC2 + SBK3 + NEK1 + LOC100996351 + SNPH + ABCG1 + PPM1K + PAGE4 + ATP1A2 + PRR15 + ZNF654 + MIR4533 + LINC02365 + PIWIL3 + MED26 + COL22A1 + IGFBP5

On choisit ensuite de forcer l'ajout de variables qualitatives qui pourraient avoir un effet sur la mort à 24 mois, le sexe, l'histologie, et l'état du patient au début de l'étude (tnm_stage), variable qui a été recodée.
Ce modèle nous permet d'obtenir un score de 0.36 sur codalab. Ce qui est très décevant.


Enfin nous avons réentrainé notre modèle sur la population totale, une fois que les NA de la variable **mort à 24 mois** aient été imputés. La méthodologie principale et resté la même, nous obtenons cette fois le modèle suivant : GIPC2 + MIR6737 + ENTPD2 + LVRN + SYT8 + TRPC2 + ARPP21 + GUF1 + SNPH + NPRL2 + FCAMR + LINC02291 + EBF1 + ATP1A2 + ZNF654 + MIR4533 + LINC02365 + MED26 +  + NKIRAS1  + tnm_stage+histology

Le modèle est légèrement différent, et meilleur. Nous améliorons sur codalab notre score de 0.03 points. Ce qui est logique mais qui a moins de sens car nous avons créé de la donnée en imputant les NA par modèle de Cox.